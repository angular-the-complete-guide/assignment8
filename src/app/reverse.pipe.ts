import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {
  transform(reverseString: any): any {
    return [...reverseString].reverse().join('');
  }
}
